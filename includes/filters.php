<?php
/**
 * Show yaad transaction error.
 *
 * @since 1.0
 * @return bool
 */
function give_yaad_show_error( $content ) {
	if (
		! isset( $_GET['give-yaad-payment'] )
		|| 'failed' !== $_GET['give-yaad-payment']
		|| ! isset( $_GET['give-yaad-error-message'] )
		|| empty( $_GET['give-yaad-error-message'] )
		|| ! give_is_failed_transaction_page()
	) {
		return $content;
	}

	return Give_Notices::print_frontend_notice(
		sprintf(
			'Payment Error: %s',
			base64_decode( $_GET['give-yaad-error-message'] )
		),
		false,
		'error'
	) . $content;
}

add_filter( 'the_content', 'give_yaad_show_error' );
