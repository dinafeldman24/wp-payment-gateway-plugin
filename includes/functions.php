<?php
/**
 * Check if yaad gateways in test mode or not
 *
 * @since 1.0
 *
 * @return bool
 */
function give_yaad_is_test_mode() {
	return apply_filters( 'give_yaad_is_test_mode', give_is_test_mode() );
}

/**
 * Get yaad api url.
 *
 * @since 1.0
 *
 * @param string $type API query type
 *
 * @return string
 */
function give_yaad_get_api_url( $type = 'processTransaction', $purchase_data ) {
	$yaad_redirect = 'https://icom.yaad.net/cgi-bin/yaadpay/yaadpay3new.pl';

	return apply_filters( 'give_yaad_get_api_url', $yaad_redirect );
}

/**
 * Get payment method label.
 *
 * @since 1.0
 *
 * @return string
 */
function give_yaad_get_payment_method_label() {
	$checkout_label = give_get_option( 'yaad_checkout_label', '' );

	return ( empty( $checkout_label )
		? __( 'yaad', 'give-yaad' )
		: $checkout_label
	);
}


/**
 * Get yaad merchant credentials.
 *
 * @since 1.0
 * @return array
 */
function give_yaad_get_merchant_credentials() {
	$credentials = array(
		'merchant_id'   => give_get_option( 'yaad_sandbox_merchant_id', '' ),
		'merchant_key'  => give_get_option( 'yaad_sandbox_mer_access_key', '' ),
		'website_name'  => give_get_option( 'yaad_sandbox_website_name', '' ),
		'industry_type' => give_get_option( 'yaad_sandbox_industry_type', '' ),
	);

	if ( ! give_yaad_is_test_mode() ) {
		$credentials = array(
			'merchant_id'   => give_get_option( 'yaad_live_merchant_id', '' ),
			'merchant_key'  => give_get_option( 'yaad_live_mer_access_key', '' ),
			'website_name'  => give_get_option( 'yaad_live_website_name', '' ),
			'industry_type' => give_get_option( 'yaad_live_industry_type', '' ),
		);
	}

	return $credentials;

}
