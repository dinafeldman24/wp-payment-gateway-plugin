<?php

class Give_yaad_Admin_Settins {
	/**
	 * Instance.
	 *
	 * @since  1.0
	 * @access static
	 * @var
	 */
	static private $instance;

	/**
	 * Payment gateways ID
	 *
	 * @since 1.0
	 * @var string
	 */
	private $gateways_id = '';

	/**
	 * Payment gateways label
	 *
	 * @since 1.0
	 * @var string
	 */
	private $gateways_label = '';

	/**
	 * Singleton pattern.
	 *
	 * @since  1.0
	 * @access private
	 * Give_yaad_Admin_Settins constructor.
	 */
	private function __construct() {
	}

	/**
	 * Get instance.
	 *
	 * @since  1.0
	 * @access static
	 * @return static
	 */
	static function get_instance() {
		if ( null === static::$instance ) {
			self::$instance = new static();
		}

		return self::$instance;
	}

	/**
	 * Setup hooks
	 *
	 * @since  1.0
	 * @access public
	 */
	public function setup() {
		$this->gateways_id    = 'yaad';
		$this->gateways_label = __( 'yaad', 'give-yaad' );

		add_filter( 'give_payment_gateways', array( $this, 'register_gateway' ) );
		add_filter( 'give_get_settings_gateways', array( $this, 'add_settings' ) );
		add_filter( 'give_get_sections_gateways', array( $this, 'add_gateways_section' ) );
	}

	/**
	 * Registers the gateway.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param $gateways
	 *
	 * @return mixed
	 */
	public function register_gateway( $gateways ) {
		$gateways[ $this->gateways_id ] = array(
			'admin_label'    => $this->gateways_label,
			'checkout_label' => give_yaad_get_payment_method_label(),
		);

		return $gateways;
	}

	/**
	 * adds the settings to the Payment Gateways section
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param $settings
	 *
	 * @return array
	 */
	public function add_settings( $settings ) {

		if ( $this->gateways_id !== give_get_current_setting_section() ) {
			return $settings;
		}

		$yaad_settings = array(
			array(
				'id'   => $this->gateways_id,
				'type' => 'title',
			),
			array(
				'name'    => esc_html__( 'Payment Method Label', 'give-yaad' ),
				'id'      => 'yaad_checkout_label',
				'type'    => 'text',
				'default' => esc_html__( 'yaad', 'give-yaad' ),
				'desc'    => __( 'Payment method label will appear on the frontend.', 'give-yaad' ),
			),
			/*array(
				'id'   => 'yaad_sandbox_merchant_id',
				'name' => __( 'Sandbox Merchant ID', 'give-yaad' ),
				'desc' => __( 'Sandbox merchant id parameter provided by yaad', 'give-yaad' ),
				'type' => 'api_key',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_sandbox_mer_access_key',
				'name' => __( 'Sandbox Merchant Key', 'give-yaad' ),
				'desc' => __( 'Sandbox secret key parameter provided by yaad', 'give-yaad' ),
				'type' => 'api_key',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_sandbox_website_name',
				'name' => __( 'Sandbox Website Name', 'give-yaad' ),
				'desc' => __( 'Sandbox website parameter provided by yaad', 'give-yaad' ),
				'type' => 'text',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_sandbox_industry_type',
				'name' => __( 'Sandbox Industry Type', 'give-yaad' ),
				'desc' => __( 'Sandbox industry type parameter provided by yaad (Retail, Entertainment, etc.)', 'give-yaad' ),
				'type' => 'text',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_live_merchant_id',
				'name' => __( 'Live Merchant ID', 'give-yaad' ),
				'desc' => __( 'Live merchant id parameter provided by yaad', 'give-yaad' ),
				'type' => 'api_key',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_live_mer_access_key',
				'name' => __( 'Live Merchant Key', 'give-yaad' ),
				'desc' => __( 'Live secret key parameter provided by yaad', 'give-yaad' ),
				'type' => 'api_key',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_live_website_name',
				'name' => __( 'Live Website Name', 'give-yaad' ),
				'desc' => __( 'Live website parameter provided by yaad', 'give-yaad' ),
				'type' => 'text',
				'size' => 'regular',
			),
			array(
				'id'   => 'yaad_live_industry_type',
				'name' => __( 'Live Industry Type', 'give-yaad' ),
				'desc' => __( 'Live industry type parameter provided by yaad (Retail, Entertainment, etc.)', 'give-yaad' ),
				'type' => 'text',
				'size' => 'regular',
			),
			array(
				'title'       => __( 'Collect Billing Details', 'give-yaad' ),
				'id'          => 'yaad_billing_details',
				'type'        => 'radio_inline',
				'options'     => array(
					'enabled'  => esc_html__( 'Enabled', 'give-yaad' ),
					'disabled' => esc_html__( 'Disabled', 'give-yaad' ),
				),
				'default'     => 'disabled',
				'description' => __( 'This option will enable the billing details section for yaad which requires the donor\'s address to complete the donation. These fields are not required by yaad to process the transaction, but you may have the need to collect the data.', 'give-yaad' ),
			),
			array(
				'id'   => $this->gateways_id,
				'type' => 'sectionend',
			),*/
		);

		return $yaad_settings;
	}

	/**
	 * Add gateway section
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param $section
	 *
	 * @return mixed
	 */
	public function add_gateways_section( $section ) {
		$section[ $this->gateways_id ] = __( 'yaad', 'give-yaad' );

		return $section;
	}
}


// Initialize settings.
Give_yaad_Admin_Settins::get_instance()->setup();
