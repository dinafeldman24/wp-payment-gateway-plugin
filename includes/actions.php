<?php
/**
 * Print cc field in donation form conditionally.
 *
 * @since 1.0
 *
 * @param $form_id
 *
 * @return bool
 */
function give_yaad_cc_form_callback( $form_id ) {
	
		give_default_cc_address_fields( $form_id );

		return true;
}

add_action( 'give_yaad_cc_form', 'give_yaad_cc_form_callback' );
