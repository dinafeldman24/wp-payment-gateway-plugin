<?php
/**
 * Listen yaad ipn
 *
 * @since  1.0
 */
function give_yaad_listen_ipn() {

	if ( isset( $_GET['give-listener'] ) && $_GET['give-listener'] == 'yaad_IPN' ) {
		do_action( 'give_verify_yaad_gateway_ipn' );
	}
}

add_action( 'init', 'give_yaad_listen_ipn' );

/**
 * Process yaad ipn
 *
 * @since  1.0
 */
function give_yaad_process_ipn() {
	if (isset( $_GET['Order'] )) {
		$info=explode("A",urldecode($_GET['Order']));
		$donation_id = $info[0];
		$form_id     = $info[1];
    }
	// Bailout.
	if (
		empty( $donation_id )
		|| empty( $form_id )
		
	) {
		exit();
	}

		if (
				$_GET['CCode']==0
		) {
			give_insert_payment_note( $donation_id, sprintf( __( 'Transaction Successful. yaad Transaction ID: %s', 'give-yaad' ), $_POST['TXNID'] ) );
			give_set_payment_transaction_id( $donation_id, $_POST['TXNID'] );
			update_post_meta( $donation_id, 'yaad_donation_response', $_GET['CCode'] );
			give_update_payment_status( $donation_id, 'complete' );
			give_send_to_success_page();
		}
	

	
	give_record_gateway_error(
		__( 'yaad Error', 'give-yaad' ),
		sprintf( __( 'Transaction Failed. yaad response message: %s', 'give-yaad' ), "Error code: ".$_GET['CCode'] ) . '<br><br>' . sprintf( esc_attr__( 'Details: %s', 'give-yaad' ), '<br>' . print_r( $_REQUEST, true ) ),
		$donation_id
	);

	give_update_payment_status( $donation_id, 'failed' );
	update_post_meta( $donation_id, 'yaad_donation_response', $_POST );
	give_insert_payment_note( $donation_id, sprintf( __( 'Transaction Failed.yaad response message:  %s', 'give-yaad' ), $response_msg ) );

	wp_redirect( give_get_failed_transaction_uri() . '?give-yaad-payment=failed&give-yaad-error-message=' . base64_encode( $response_msg ) );
	exit();
}

add_action( 'give_verify_yaad_gateway_ipn', 'give_yaad_process_ipn' );


/**
 * Processes the payment
 *
 * @since  1.0
 *
 * @param $purchase_data
 */
function give_yaad_process_payment( $purchase_data ) {
	$yaad_redirect = give_yaad_get_api_url('processTransaction', $purchase_data);
    
	// check for any stored errors
	$errors = give_get_errors();
	if ( ! $errors ) {

		/**
		 **************************************
		 * setup the payment details to be stored
		 */
		// setup the payment details
		$payment_data = array(
			'price'           => $purchase_data['price'],
			'give_form_title' => $purchase_data['post_data']['give-form-title'],
			'give_form_id'    => (int) $purchase_data['post_data']['give-form-id'] ,
			'give_price_id'   => isset( $purchase_data['post_data']['give-price-id'] ) ? $purchase_data['post_data']['give-price-id'] : '',
			'date'            => $purchase_data['date'],
			'user_email'      => $purchase_data['user_email'],
			'purchase_key'    => $purchase_data['purchase_key'],
			'currency'        => give_get_currency(),
			'user_info'       => $purchase_data['user_info'],
			'status'          => 'pending',
			'gateway'         => $purchase_data['gateway'],
		);

		// Record the pending payment
		$payment = give_insert_payment( $payment_data );

		// Verify donation payment.
		if ( ! $payment ) {
			// Record the error.
			give_record_gateway_error(
				esc_html__( 'Payment Error', 'give-paytm' ),
				/* translators: %s: payment data */
				sprintf(
					esc_html__( 'Payment creation failed before process Yaad gateway. Payment data: %s', 'give-paytm' ),
					json_encode( $purchase_data )
				),
				$payment
			);

			// Problems? Send back.
			give_send_back_to_checkout( '?payment-mode=' . $purchase_data['post_data']['give-gateway'] );
		}	

		// Set yaad query params.
		if ($purchase_data['post_data']['give-cs-currency']=='USD')$currency=2;
		if ($purchase_data['post_data']['give-cs-currency']=='ILS')$currency=1;
		$params     = array(
			'action'              => 'pay',
			'PassP'               => 'pb',
			'Masof'               => '4500626122',
			'Order'				  => urlencode($payment."A".$purchase_data['post_data']['give-form-id']),
			'sendemail'           => 'True',
			'UTF8' 		          => 'True',
			'UTF8out' 	          => 'True',
			'ClientName'       	  => $purchase_data['user_info']['first_name'],
			'ClientLName'     	  => $purchase_data['user_info']['last_name'],
			'phone'		   		  => $purchase_data['post_data']['phone'],
			'email'            	  => $purchase_data['user_email'],
			'Amount'       		  => $purchase_data['price'],
			'Info'          	  => $purchase_data['post_data']['give-form-title'],
			'tmp'          		  => '8',
			'Coin'                => $currency,
			'PageLang'            => 'ENG',
			'Postpone'            => 'False',
			'Tash'                => '1',
			'FixTash'			  => 'False',
			'SendHesh' 			  => 'True',
			//'CALLBACK_URL' => get_site_url() . "/?give-listener=yaad_IPN&Order={$payment}|{$purchase_data['post_data']['give-form-id']}"
		);

		// Add billing info if admin want to collect donor billing address.
		
			$params['street'] = $purchase_data['post_data']['card_address'];
			$params['city']      = $purchase_data['post_data']['card_city'];
			$params['zip']   = $purchase_data['post_data']['card_zip'];
		

		/**
		 * Filter the yaad transaction request params
		 *
		 * @since 1.0
		 *
		 * @param array $params
		 */
		$params = apply_filters( 'give_yaad_transaction_request_params', $params );

		$submit_Params          = '';

		foreach ( $params as $key => $val ) {
			$submit_Params .= trim( $key ) . '=' . trim( urlencode( $val ) ) . '&';
		}

		$submit_Params = substr( $submit_Params, 0, - 1 );
		
		wp_redirect(get_site_url()."/your-billing-information?url=".$yaad_redirect ."&".$submit_Params);
		//wp_redirect($yaad_redirect ."?".$submit_Params);
		//echo file_get_contents(get_site_url()."/your-billing-information?url=".$yaad_redirect ."&".$submit_Params);
		exit();
	}// End if().

	// if errors are present, send the user back to the purchase page so they can be corrected
	give_send_back_to_checkout( "?payment-mode={$purchase_data['gateway']}&form-id={$purchase_data['post_data']['give-form-id']}" );
}

add_action( 'give_gateway_yaad', 'give_yaad_process_payment' );

