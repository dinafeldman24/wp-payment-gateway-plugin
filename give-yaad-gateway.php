<?php
/**
 * Plugin Name: Yaadpay Gateway for Give
 * Description: Integrates Give Forms with yaadpay Method.
 * Version: 1.1.1
 * Author: Dina Feldman
 * Requires at least: 3.5
 * Tested up to: 4.3
 *
 * Text Domain: give-yaad-gateway
 * Domain Path: /lang/
 *
 * @package yaadpay Form Gateway for Give
 */

define( 'GF_yaadPAY_FORM_VERSION', '1.1.0' );

if ( ! class_exists( 'Give_yaad' ) ) {
	final class Give_yaad {
		/**
		 * Instance.
		 *
		 * @since  1.0
		 * @access static
		 * @var
		 */
		static private $instance;

		/**
		 * Notices (array)
		 *
		 * @since 1.0.1
		 *
		 * @var array
		 */
		public $notices = array();

		/**
		 * Singleton pattern.
		 *
		 * @since  1.0
		 * @access private
		 */
		private function __construct() {
		}


		/**
		 * Get instance.
		 *
		 * @since  1.0
		 * @access static
		 * @return static
		 */
		static function get_instance() {
			if ( null === static::$instance ) {
				self::$instance = new self();
				self::$instance->setup();
			}

			return self::$instance;
		}


		/**
		 * Setup Give yaad.
		 *
		 * @since  1.0.1
		 * @access private
		 */
		private function setup() {

			// Setup constants.
			$this->setup_constants();

			// Give init hook.
			add_action( 'give_init', array( $this, 'init' ), 10 );
			add_action( 'admin_init', array( $this, 'check_environment' ), 999 );
			add_action( 'admin_notices', array( $this, 'admin_notices' ), 15 );
		}

		/**
		 * Setup constants.
		 *
		 * @since  1.0
		 * @access public
		 * @return Give_Payumoney_Gateway
		 */
		public function setup_constants() {
			// Global Params.
			define( 'GIVE_yaad_VERSION', '1.1.0' );
			define( 'GIVE_yaad_MIN_GIVE_VER', '2.4.1' );
			define( 'GIVE_yaad_BASENAME', plugin_basename( __FILE__ ) );
			define( 'GIVE_yaad_URL', plugins_url( '/', __FILE__ ) );
			define( 'GIVE_yaad_DIR', plugin_dir_path( __FILE__ ) );

			return self::$instance;
		}

		/**
		 * Load the text domain.
		 *
		 * @access private
		 * @since  1.0
		 *
		 * @return void
		 */
		public function load_textdomain() {

			// Set filter for plugin's languages directory.
			$give_yaad_lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$give_yaad_lang_dir = apply_filters( 'give_yaad_languages_directory', $give_yaad_lang_dir );

			// Traditional WordPress plugin locale filter
			$locale = apply_filters( 'plugin_locale', get_locale(), 'give-yaad' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'give-yaad', $locale );

			// Setup paths to current locale file
			$mofile_local  = $give_yaad_lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/give-yaad/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/give-yaad folder
				load_textdomain( 'give-yaad', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/give-yaad/languages/ folder
				load_textdomain( 'give-yaad', $mofile_local );
			} else {
				// Load the default language files
				load_plugin_textdomain( 'give-yaad', false, $give_yaad_lang_dir );
			}

		}

		/**
		 * Set hooks
		 *
		 * @since  1.0.1
		 * @access public
		 */
		public function init() {

			if ( ! $this->get_environment_warning() ) {
				return;
			}

			$this->load_textdomain();

			if ( is_admin() ) {
				require_once GIVE_yaad_DIR . 'includes/admin/plugin-activation.php';
			} elseif ( ! function_exists( 'sanitizedParam' ) ) {
				require_once GIVE_yaad_DIR . 'includes/lib/encdec_yaad.php';
			}

			require_once GIVE_yaad_DIR . 'includes/functions.php';

			require_once GIVE_yaad_DIR . 'includes/admin/class-admin-settings.php';
			require_once GIVE_yaad_DIR . 'includes/payment-processing.php';
			require_once GIVE_yaad_DIR . 'includes/actions.php';
			require_once GIVE_yaad_DIR . 'includes/filters.php';
		}


		/**
		 * Check plugin environment.
		 *
		 * @since  1.0.1
		 * @access public
		 *
		 * @return bool
		 */
		public function check_environment() {
			// Flag to check whether plugin file is loaded or not.
			$is_working = true;

			// Load plugin helper functions.
			if ( ! function_exists( 'is_plugin_active' ) ) {
				require_once ABSPATH . '/wp-admin/includes/plugin.php';
			}

			/*
			 Check to see if Give is activated, if it isn't deactivate and show a banner. */
			// Check for if give plugin activate or not.
			$is_give_active = defined( 'GIVE_PLUGIN_BASENAME' ) ? is_plugin_active( GIVE_PLUGIN_BASENAME ) : false;

			if ( empty( $is_give_active ) ) {
				// Show admin notice.
				$this->add_admin_notice( 'prompt_give_activate', 'error', sprintf( __( '<strong>Activation Error:</strong> You must have the <a href="%s" target="_blank">Give</a> plugin installed and activated for Give - yaad to activate.', 'give-yaad' ), 'https://givewp.com' ) );
				$is_working = false;
			}

			return $is_working;
		}

		/**
		 * Check plugin for Give environment.
		 *
		 * @since  1.0.1
		 * @access public
		 *
		 * @return bool
		 */
		public function get_environment_warning() {
			// Flag to check whether plugin file is loaded or not.
			$is_working = true;

			// Verify dependency cases.
			if (
				defined( 'GIVE_VERSION' )
				&& version_compare( GIVE_VERSION, GIVE_yaad_MIN_GIVE_VER, '<' )
			) {

				/*
				 Min. Give. plugin version. */
				// Show admin notice.
				$this->add_admin_notice( 'prompt_give_incompatible', 'error', sprintf( __( '<strong>Activation Error:</strong> You must have the <a href="%1$s" target="_blank">Give</a> core version %2$s for the Give - yaad add-on to activate.', 'give-yaad' ), 'https://givewp.com', GIVE_yaad_MIN_GIVE_VER ) );

				$is_working = false;
			}

			return $is_working;
		}

		/**
		 * Allow this class and other classes to add notices.
		 *
		 * @since  1.0.1
		 *
		 * @param $slug
		 * @param $class
		 * @param $message
		 */
		public function add_admin_notice( $slug, $class, $message ) {
			$this->notices[ $slug ] = array(
				'class'   => $class,
				'message' => $message,
			);
		}

		/**
		 * Display admin notices.
		 *
		 * @since  1.0.1
		 */
		public function admin_notices() {

			$allowed_tags = array(
				'a'      => array(
					'href'  => array(),
					'title' => array(),
					'class' => array(),
					'id'    => array(),
				),
				'br'     => array(),
				'em'     => array(),
				'span'   => array(
					'class' => array(),
				),
				'strong' => array(),
			);

			foreach ( (array) $this->notices as $notice_key => $notice ) {
				echo "<div class='" . esc_attr( $notice['class'] ) . "'><p>";
				echo wp_kses( $notice['message'], $allowed_tags );
				echo '</p></div>';
			}

		}

		/**
		 * Show activation banner for this add-on.
		 *
		 * @since  1.0.1
		 *
		 * @return bool
		 */
		public function activation_banner() {

			// Check for activation banner inclusion.
			if (
				! class_exists( 'Give_Addon_Activation_Banner' )
				&& file_exists( GIVE_PLUGIN_DIR . 'includes/admin/class-addon-activation-banner.php' )
			) {
				include GIVE_PLUGIN_DIR . 'includes/admin/class-addon-activation-banner.php';
			}

			// Initialize activation welcome banner.
			if ( class_exists( 'Give_Addon_Activation_Banner' ) ) {

				// Only runs on admin.
				$args = array(
					'file'              => __FILE__,
					'name'              => esc_html__( 'yaad Gateway', 'give-yaad' ),
					'version'           => GIVE_yaad_VERSION,
					'settings_url'      => admin_url( 'edit.php?post_type=give_forms&page=give-settings&tab=gateways&section=yaad' ),
					'documentation_url' => 'http://docs.givewp.com/addon-yaad',
					'support_url'       => 'https://givewp.com/support/',
					'testing'           => false, // Never leave true.
				);
				new Give_Addon_Activation_Banner( $args );
			}

			return true;
		}
	}

	/**
	 * Returns class object instance.
	 *
	 * @since 1.0.1
	 *
	 * @return Give_yaad bool|object
	 */
	function Give_yaad() {
		return Give_yaad::get_instance();
	}

	Give_yaad();
}
